﻿using Commons.Log;
using Pomodoro.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Pomodoro
{
    public class PomodoroTimer
    {
        private Object stateLocker = new object();

        private States state = States.Stoped;
        /// <summary>
        /// duration in seconds
        /// </summary>
        private int duration = 1500;

        /// <summary>
        /// min short break time duration in seconds
        /// </summary>
        private int minShortBreakTime = 180;

        /// <summary>
        /// max short break time duration in seconds
        /// </summary>
        private int maxShortBreakTime = 300;

        /// <summary>
        /// Number of cycles before a long break;
        /// </summary>
        private int cyclesForLongBreak = 4;

        private int cyclesFinished = 0;

        DateTime taskStartTime;

        public PomodoroTimer()
        {
            IsFinished = false;
        }

        public bool IsFinished
        {
            get;
            set;
        }

        public void Run()
        {
            LogManager.LogInfo("Pomodoro started");

            state = States.StartRequested;

            while (GetState() != States.Closed)
            {
                DoProcess();
                System.Threading.Thread.Sleep(50);
            }
        }

        private void DoProcess()
        {
            double elapsedTime = (DateTime.UtcNow - taskStartTime).TotalSeconds;

            UpdateState(elapsedTime);
            switch (GetState())
            {
                case States.StartRequested:
                    Start();
                    break;
                case States.Running:
                    ProcessWorkTime(elapsedTime);
                    break;
                case States.ShortBreakRequested:
                    ProcessShortBreakRequest();
                    break;
                case States.ShortBreak:
                    ViewFacade.UpdateBreakView((int)(maxShortBreakTime-elapsedTime));
                    break;
            }
        }

        private void UpdateState(double elapsedTime)
        {
            switch (state)
            {
                case States.Running:

                    if (elapsedTime > duration)
                    {
                        if (cyclesFinished >= cyclesForLongBreak)
                        {
                            SetState(States.StopRequested);
                        }
                        else
                        {
                            SetState(States.ShortBreakRequested);
                            ++cyclesFinished;
                        }
                    }

                    break;
                case States.ShortBreak:

                    if (elapsedTime > maxShortBreakTime)
                    {
                        SetState(States.StartRequested);
                    }

                    break;
            }
        }

        private void Start()
        {
            LogManager.LogInfo("Work time started");

            ViewFacade.ShowRunningView();

            taskStartTime = DateTime.UtcNow;

            SetState(States.Running);
        }

        private void ProcessWorkTime(double elapsedTime)
        {
            ViewFacade.UpdateRunningView((int)elapsedTime);
        }

        private void ProcessShortBreakRequest()
        {
            taskStartTime = DateTime.UtcNow;

            ViewFacade.ShowBreakView();

            SetState(States.ShortBreak);
        }

        #region setters

        public void SetState(States newState)
        {
            lock (stateLocker)
            {
                LogManager.LogInfo("Setting state to " + newState);
                state = newState;
            }
        }

        public States GetState()
        {
            lock (stateLocker)
            {
                return state;
            }
        }

        public void SetDuration(int lengthInSec)
        {
            duration = lengthInSec;
        }

        public void SetMinShortBreakDuration(int lengthInSec)
        {
            minShortBreakTime = lengthInSec;
        }

        public void SetMaxShortBreakDuration(int lengthInSec)
        {
            maxShortBreakTime = lengthInSec;
        }

        public void SetCyclesBeforeLongBreak(int number)
        {
            cyclesForLongBreak = number;
        }

        #endregion
    }
}
