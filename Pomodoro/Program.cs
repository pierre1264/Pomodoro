﻿using Commons.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pomodoro
{
    class Program
    {

        [STAThread]
        static void Main(string[] args)
        {
            LogManager.AddListener(new FileLogWriter());
            PomodoroTimer pomodoro = new PomodoroTimer();
            pomodoro.SetDuration(5);
            pomodoro.Run();

            while(!pomodoro.IsFinished)
            {
                Thread.Sleep(1000);
            }
        }
    }
}
