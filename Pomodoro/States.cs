﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pomodoro
{
    public enum States
    {
        StartRequested,
        Running,
        ShortBreakRequested,
        ShortBreak,
        StopRequested,
        Stoped,
        Closed
    }
}
