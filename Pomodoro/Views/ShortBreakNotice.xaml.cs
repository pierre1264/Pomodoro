﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Pomodoro.Views
{
    /// <summary>
    /// Interaction logic for ShortBreakNotice.xaml
    /// </summary>
    public partial class ShortBreakNotice : Window
    {
        public ShortBreakNotice()
        {
            InitializeComponent();
        }

        public void SetClock(int seconds)
        {
            int minute = seconds / 60;

            timer.Content = minute + ":" + seconds % 60;
            this.UpdateLayout();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
        }
    }
}
