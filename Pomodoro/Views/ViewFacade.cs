﻿namespace Pomodoro.Views
{
    public static class ViewFacade
    {
        private static ShortBreakNotice shortBreakNotice = new ShortBreakNotice();
        
        public static void ShowRunningView()
        {
            shortBreakNotice.Hide();
        }

        public static void ShowBreakView()
        {
            shortBreakNotice.Show();
        }

        public static void UpdateRunningView(int remainingSeconds)
        {
        }

        public static void UpdateBreakView(int remainingSeconds)
        {
            shortBreakNotice.SetClock(remainingSeconds);
        }
    }
}
